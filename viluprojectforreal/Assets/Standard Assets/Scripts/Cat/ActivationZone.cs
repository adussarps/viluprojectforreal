﻿using UnityEngine;
using System.Collections;

public class ActivationZone : MonoBehaviour {
	public Collider pickableobject; 

	void setPosition(){
		var player =	GameObject.FindWithTag ("Player");
		Vector3 pos;
		float angle;
		player.rigidbody.rotation.ToAngleAxis (out angle, out pos);
		pos = pos / pos.magnitude;
		this.rigidbody.position = player.rigidbody.position+pos;
	
	}

	// Use this for initialization
	void Start () {
		setPosition();
		var player =	GameObject.FindWithTag ("Player");
		MovementCat cat = player.GetComponents<MovementCat> () [0];
		cat.ActTrigger = this;
	}
	
	// WhyUpdate is called once per frame
	void FixedUpdate () {
		setPosition ();
	}

	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == "Pickable") {
			pickableobject=other;
		}
	}
}
