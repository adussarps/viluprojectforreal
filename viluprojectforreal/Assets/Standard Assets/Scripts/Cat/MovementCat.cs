﻿using UnityEngine;
using System.Collections;

public class MovementCat : MonoBehaviour {

	/// <summary>
	/// Velocity from the cat
	/// </summary>
	// Use this for initialization in the interface
	public float velocity;

	// We put an activation zone, we put it public so the activationzone itself do his call on start.
	public ActivationZone ActTrigger;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		controlMovementCat ();
		if(Input.GetKey (KeyCode.E)){
			pickObject();

		}
	}

	/// <summary>
	/// Controlls the rigidbody(Catobject) with the controls
	/// </summary>
	void controlMovementCat(){
		//take the velocity from the cat
		//if we don't take the velocity from interface only start with this velocity
		this.rigidbody.velocity = Vector3.zero; // zero is Vector3(0,0,0)

		//inputs
		/// <summary>
		/// Vrigibody.velocity takes the movements frame per frame
		/// </summary>

		if (Input.GetKey (KeyCode.W)) {
			this.rigidbody.velocity+=Vector3.forward;
		}
		if (Input.GetKey (KeyCode.S)) {
			this.rigidbody.velocity+=Vector3.back;
		}
		if (Input.GetKey (KeyCode.D)) {
			this.rigidbody.velocity+=Vector3.right;
		}
		if(Input.GetKey(KeyCode.A)){
			this.rigidbody.velocity+=Vector3.left;
		}
		//how the velocity is growing we controll the velocity , restart to velocity.normalized (velocity =1)
		if(this.rigidbody.velocity.magnitude>0.5f){
			this.rigidbody.velocity=this.rigidbody.velocity.normalized*this.velocity;
		}
	}

	void pickObject(){
		//ActivationZone ActTrigger= GameObject.FindWithTag ("Activationtrigger");

	
	}




}
