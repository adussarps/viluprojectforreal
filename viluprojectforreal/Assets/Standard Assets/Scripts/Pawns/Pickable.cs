﻿using UnityEngine;
using System.Collections;

public class Pickable : MonoBehaviour {

	//Attributos de un pickable
	bool ispicked;
	//un pickable tiene que tener un rigid body para ser cogido

	void setPosition(){
		var player =	GameObject.FindWithTag ("Player");
		Vector3 pos;
		float angle;
		player.rigidbody.rotation.ToAngleAxis (out angle, out pos);
		pos = pos*3 / pos.magnitude;
		this.rigidbody.position = player.rigidbody.position+pos;
		
	}



	// Use this for initialization
	void Start () {
		ispicked = false;
	}



	// Update is called once per frame
	void Update () {
		if (ispicked) {
			setPosition();
		}
	
	}
}
