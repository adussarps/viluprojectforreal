﻿using OpenTK;
using System;
using UnrealEngine.Runtime;
using UnrealEngine.Engine;


namespace FinalTestMono
{
	public class Cat : Character
	{
		// Constructor called when creating or loading an object
		protected Cat(PostConstructInitializeProperties pcip)
			: base(pcip)
		{ 
			//Aqui vienen un monton de opciones del mesh y los valores iniciales de los campo de estastisticas del gato...
		/*	MeshComponent = pcip.CreateDefaultSubobject<SkeletalMeshComponent>("CatPlayerMesh");
			SetRootComponent(MeshComponent); */

			// Set the size of the collision capsule.
			//a modificar
			CapsuleComponent.Object.SetCapsuleSize (42.0f, 96.0f);

			// Tambien la de la capsula de interaccion
			CapsuleInteraction.Object.SetCapsuleSize (10.0f, 10.0f);
			// Set input turn rates.
			BaseTurnRate = 45.0f;
			BaseLookUpRate = 45.0f;

			// Rotate the camera with the controller, not the character.
			UseControllerRotationPitch = false;
			UseControllerRotationYaw = true;
			UseControllerRotationRoll = false;

			// Configure the character's movement

			CharacterMovement.Object.OrientRotationToMovement = true;  // Rotate in direction of movement...
			CharacterMovement.Object.RotationRate = new Rotator (0.0f, 576.0f, 0.0f);    // ...at this rate.
			CharacterMovement.Object.JumpZVelocity = 600.0f;
			CharacterMovement.Object.AirControl = 0.2f;
			CharacterMovement.Object.NavAgentProps = new NavAgentProperties {
				AgentRadius = 42.0f,
				AgentHeight = 192.0f,


			};


			// Create a camera boom which pulls in towards the player if there's a collision
			CameraBoom = pcip.CreateDefaultSubobject<SpringArmComponent> ("CameraBoom");
			CameraBoom.Object.AttachTo (RootComponent);
			CameraBoom.Object.TargetArmLength = 300.0f;    // Follow distance behind the character
			CameraBoom.Object.UseControllerViewRotation = true;   // Rotate the arm based on the controller

			// Create the follow camera and attach it to the boom.
			FollowCamera = pcip.CreateDefaultSubobject<CameraComponent> ("FollowCamera");
			FollowCamera.Object.AttachTo (CameraBoom);
			FollowCamera.Object.UseControllerViewRotation = true; // Camera doesn't rotate relative to the arm






			Mesh.Object.OnlyOwnerSee = false;
			Mesh.Object.OwnerNoSee = false;
			Mesh.Object.ReceivesDecals = false;

			//Inizializamos las propriedades del 
			Velocity = 999.0f;
			RunVelocityCoef = 2.5f;
			RealVelocity = Velocity;
			CharacterMovement.Object.MaxWalkSpeed = RealVelocity;
			//CharacterMovement.Object.Velocity

		}

		// Constructor for hot-reloading. UProperties will already be initialized, but any purely managed fields or data
		// will need to be set up
		protected Cat(IntPtr nativeObject)
			: base(nativeObject)
		{

		}
		//This class is defined especialy fot our main character...(the cat)
	//We declare the diferent general properties of our character here.
	//here comes the mesh
		/*
		[UProperty, BlueprintVisible (ReadOnly = true), EditorVisible (ReadOnly = true)]
		[Category ("Static Mesh")]
		[UMetaData ("ExposeFunctionCategories", "Mesh,Rendering,Physics,Components|StaticMesh")]
		public Subobject<SkeletalMeshComponent> MeshComponent { get; private set; }
		*/
		[UProperty, BlueprintVisible (ReadOnly = true), EditorVisible (ReadOnly = true)]
		[Category ("Capsule")]

		public Subobject<CapsuleComponent> CapsuleInteraction { get; private set; }



		/*
		-vida
		-experience
		-velocity
		-force
		-lifeBar
		-damage
		-Atacktime
				*/
		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Movement")]
		public float BaseTurnRate { get; set; }

		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Movement")]
		public float BaseLookUpRate { get; set; }


		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Stats")]
		public Int16 Life{ get; set; }

		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Experience")]
		public int Experience { get; set; }

		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Stats")]
		public float Velocity { get; set; }



		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Movement")]
		public float RunVelocityCoef { get; set; }



		//Este valor sirve para definir la velocidad actual del gato. No es una estatistica...quizas hacemos un struct de stats...
		//O una clase que lleva los stats y una subclase que lleva los metodos y los caracteristicas ingame?... 
		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Movement")]
		public float RealVelocity { get; set; }



		//Aqui ponemos la velocidad del gato cuando core.
		// int RunningVelocity = (int) (RunVelocityCoef * (float)Velocity);
		//no se puede -_-  -> lo tenemos que poner dentro de run entonces
		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Stats")]
		public int Strenght { get; set; }
	
	
		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Bar")]
		public int LifeBar { get; set; }
	
		//Poner para que sirve...
		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Damage")]
		public int Damage { get; set; }

		[UProperty, BlueprintVisible, EditorVisible]
		[Category ("Delay")]
		public float AttackTime { get; set; }


		/* Parametros de la camara y cosas asi :)*/

		[UProperty, BlueprintVisible (ReadOnly = true), EditorVisible (ReadOnly = true)]
		[Category ("Camera")]
		public Subobject<SpringArmComponent> CameraBoom { get; set; }

		[UProperty, BlueprintVisible (ReadOnly = true), EditorVisible (ReadOnly = true)]
		[Category ("Camera")]
		public Subobject<CameraComponent> FollowCamera { get; set; }









		public void Walk(){
			this.RealVelocity = Velocity;
			CharacterMovement.Object.MaxWalkSpeed = RealVelocity;
		
		}


		public void Run(){
			float RunningVelocity = (float) (RunVelocityCoef * (float)Velocity);
			this.RealVelocity = RunningVelocity;
			CharacterMovement.Object.MaxWalkSpeed =RunningVelocity;
		
		}




		protected override void BindInput (InputComponent enabledInputComponent)
		{
			base.BindInput (enabledInputComponent);

			// Bind the jump action to Character.Jump(), which already has the appropriate delegate signature.
			enabledInputComponent.BindAction (InputAction.Jump, InputEvent.Pressed, JumpCat);
			enabledInputComponent.BindAction (InputAction.Run, InputEvent.Pressed, Run);
			enabledInputComponent.BindAction (InputAction.Run, InputEvent.Released, Walk);

			enabledInputComponent.BindAxis (InputAxis.MoveForward, MoveForward);
			enabledInputComponent.BindAxis (InputAxis.MoveRight, MoveRight);

			// Bind the camera controls two ways.  For example, we'd treat a mouse as an absolute delta,
			// but impose a fixed turn rate on an analog stick.
			enabledInputComponent.BindAxis (InputAxis.Turn, AddControllerYawInput);
			enabledInputComponent.BindAxis (InputAxis.TurnRate, (float axisValue) => AddControllerYawInput (World.GetWorldDeltaSeconds () * BaseTurnRate * axisValue));
			enabledInputComponent.BindAxis (InputAxis.LookUp, AddControllerPitchInput);
			enabledInputComponent.BindAxis (InputAxis.LookUpRate, (float axisValue) =>  AddControllerPitchInput (World.GetWorldDeltaSeconds () * BaseLookUpRate * axisValue));

			// Add a handler to translate taps into actions, for touch devices.
			enabledInputComponent.BindTouch (InputEvent.Pressed, OnTouched);
		}


		private void OnTouched (UnrealEngine.InputCore.TouchIndex touchIndex, Vector3 location)
		{
			// Jump, but only on the first tap.
			if (touchIndex == UnrealEngine.InputCore.TouchIndex.Touch1) {
				JumpCat ();
			}
		}

		private void MoveForward (float axisValue)
		{
			// Find out which way is forward, but move only in one plane.
			Rotator rot = GetControlRotation ();
			rot.Pitch = rot.Roll = 0.0f;
			Vector3 forwardVector = rot.GetForwardVector ();

			AddMovementInput (forwardVector, RealVelocity*axisValue);
		}

		private void MoveRight (float axisValue)
		{
			// Find out which way is right, but move only in one plane.
			Rotator rot = GetControlRotation ();
			rot.Pitch = rot.Roll = 0.0f;
			Vector3 rightVector = rot.GetRightVector ();

			AddMovementInput (rightVector, axisValue*RealVelocity);
		}



		[UFunction,BlueprintCallable]
		[Category("Movement")]
		public void JumpCat(){
			//Aqui calculamos el tipo de salto que sera : salto normal o salto para agriparse , o salto fallado , dependiendo de la posicion
			//se llama con espacio?






			//Attack sera parte de esto o no?
		

			this.Jump ();
		
		}


		[UFunction,BlueprintCallable]
		[Category("Movement")]
		public void Attack(){
			//se llama con click derecha?
			//a pensar un poco mas... como ataca el gato?
			//yo veo un gameplay a la assassins creed...

		}

		[UFunction,BlueprintCallable]
		[Category("Movement")]
		public void Interact(){
			//Mira si hay objeto intercatuable enfente

			//Si hay mas de uno elegir el mas cerca

			//actua de verdad
		}


		[UFunction,BlueprintCallable]
		[Category("Movement")]
		public void Eat(){
			//Si es objeto interctualble y que queremos comerlo... misma methodo que con actuable pero if eatable
		}




	}
}
